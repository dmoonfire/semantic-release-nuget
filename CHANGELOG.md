## [1.1.1](https://gitlab.com/dmoonfire/semantic-release-nuget/compare/v1.1.0...v1.1.1) (2021-09-11)


### Bug Fixes

* force a clean and build before pack ([23112ca](https://gitlab.com/dmoonfire/semantic-release-nuget/commit/23112cab2c0efc7a631937bb1a70fb2c74ab49fb))

# [1.1.0](https://gitlab.com/dmoonfire/semantic-release-nuget/compare/v1.0.2...v1.1.0) (2021-09-04)


### Bug Fixes

* creating sane defaults for `dotnet` ([fd4b9e3](https://gitlab.com/dmoonfire/semantic-release-nuget/commit/fd4b9e39bcc1b01bbef04274998f3f258491bab1))


### Features

* changed pushFiles to be an array ([e01246e](https://gitlab.com/dmoonfire/semantic-release-nuget/commit/e01246e1c300fdaad2d5f4f17695b86dc0a4fc29))

## [1.0.2](https://gitlab.com/dmoonfire/semantic-release-nuget/compare/v1.0.1...v1.0.2) (2021-09-04)


### Bug Fixes

* adding more diagnostics on commands ([1b56849](https://gitlab.com/dmoonfire/semantic-release-nuget/commit/1b56849f3a7ef18b30ae06c9ea662ad73ce76a52))

## [1.0.1](https://gitlab.com/dmoonfire/semantic-release-nuget/compare/v1.0.0...v1.0.1) (2021-09-04)


### Bug Fixes

* removing aggregate-error ([5ec98b8](https://gitlab.com/dmoonfire/semantic-release-nuget/commit/5ec98b884a430464b93cc8db9f22c5495d3b173f))

# 1.0.0 (2021-09-04)


### Features

* initial commit ([a03c00f](https://gitlab.com/dmoonfire/semantic-release-nuget/commit/a03c00f2b1dec94e84660802bef0fe388af6be82))

# 1.0.0 (2021-08-30)


### Bug Fixes

* updating package information ([0ca3171](https://gitlab.com/dmoonfire/semantic-release-dotnet/commit/0ca3171bb1f261bf89a0e561baa9b0d12ceb8c65))
