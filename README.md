semantic-release-nuget
=======================

`semantic-release-nuget` is a [semantic-release](https://semantic-release.gitbook.io/semantic-release/) plugin for creating NuGet packages for .NET during the release process. It uses the following steps:

- verifyConditions: Make sure settings are right and `dotnet nuget --version` works.
- publish: Publish the packages to the repository.

## Installing

Depending on your package manager, you'll need to run one of the commands.

```
npm install semantic-release-nuget --only=dev
yarn add semantic-release-nuget -D
```

Typically this is only used as a development dependency.

## Usage

Add the plugin into the `release.config.js` file or in the appropriate section of `package.json`. An example `release.config.js`:

```
module.exports = {
    plugins: [
        "@semantic-release/commit-analyzer",
        "semantic-release-nuget",
    ],
};
```

The default is to run `dotnet pack` and `dotnet push *.nukpkg`.

Options can be given also:

```
module.exports = {
    plugins: [
        "@semantic-release/commit-analyzer",
        [
            "semantic-release-dotnet",
            {
                packArguments: ["--include-symbols", "--include-source"],
                pushFiles: "bin/*.nupkg",
            },
        ],
    ],
};
```

## Options

There are only a few options for the command.

### `pushUrl?: string`

* Required
* Default: `NUGET_PUSH_URL` environment variable

This is the URL to push up the NuGet package. If it isn't set in the release configuration, then it needs to be set in the `NUGET_PUSH_URL` environment variable. If in neither, then the verify conditions step will fail with a message.

### `apiKey?: string`

* Required
* Default: `NUGET_TOKEN` environment variable

This is the API key used to push the NuGet package. If it isn't set, then `NUGET_TOKEN` will be used, otherwise an error will be thrown and the release will fail.

### `packArguments?: string[]`

* Default: `[]`

Any additional arguments to the `dotnet pack` command. For example, one could use `["--include-symbols"]` to include symbol packages.

### `pushFiles?: string[]`

* Default: `[*.nupkg]`

A list of files to include in the push. Any globs (`*` in the path) will be passed directly to `dotnet nuget push` without translation.

### `debug?: boolean`

* Default: `false`

Turns on debugging messages. This defaults to `false` to avoid information overload.
