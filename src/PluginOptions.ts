export interface PluginOptions {
    /**
     * The URL to push NuGet packages.
     */
    pushUrl: string;

    /**
     * The API key used for pushing.
     */
    apiKey: string;

    /**
     * The arguments to the `dotnet pack` command. Defaults to "-s Release".
     */
    packArguments: string[];

    /**
     * The path to the `dotnet nuget push` command. Defaults to "*.nupkg".
     */
    pushFiles: string[];

    /**
     * Set to true for debugging the library.
     */
    debug: boolean;
}
