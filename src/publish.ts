import { PluginOptions } from "./PluginOptions";
import { resolveOptions } from "./options";
import * as child from "child_process";

export async function publish(
    options: PluginOptions,
    config: any
): Promise<void> {
    // Resolve the options and make sure we have them all populated.
    const resolved = resolveOptions(options, config);

    // Set up debugging.
    const { logger } = config;
    const debug = resolved.debug ? logger.debug : () => {};
    logger.debug = debug;

    // We need to clean and rebuild.
    logger.info("Running the dotnet clean command");
    const clean = child.spawnSync("dotnet", ["clean"]);

    if (clean.status !== 0) {
        throw new Error(
            "Cannot run `dotnet clean`\n\n" + clean.stdout.toString()
        );
    }

    // We need to clean and rebuild.
    logger.info("Running the dotnet build command");
    const build = child.spawnSync("dotnet", ["build"]);

    if (build.status !== 0) {
        throw new Error(
            "Cannot run `dotnet build`\n\n" + build.stdout.toString()
        );
    }

    // Run the pack commands.
    logger.info("Running the dotnet pack command");
    const pack = child.spawnSync(
        "dotnet",
        ["pack"].concat(resolved.packArguments)
    );

    if (pack.status !== 0) {
        throw new Error(
            "Cannot run `dotnet pack`\n\n" + pack.stdout.toString()
        );
    }

    // Run the push commands.
    logger.info("Running the dotnet nuget push command");
    const push = child.spawnSync(
        "dotnet",
        ["nuget", "push"]
            .concat(resolved.pushFiles)
            .concat([
                "--api-key",
                resolved.apiKey,
                "--source",
                resolved.pushUrl,
            ])
    );

    if (push.status !== 0) {
        throw new Error(
            "Cannot run `dotnet nuget push`\n\n" + push.stdout.toString()
        );
    }
}
